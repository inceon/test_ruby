class Animal
	attr_reader :name, :kind, :price, :age, :breed, :vaccinations, :recommend
	attr_reader :purchased
	def initialize(name, kind, price, age, breed, vaccinations, recommend)
		@name = name
		@kind = kind
		@price = price
		@age = age
		@breed = breed
		@vaccinations = vaccinations
		@recommend = recommend
		@purchased = false
	end

	def purchased=(val)
		@purchased = val
	end

	def data
		return [@name, @kind, @price, @age, @breed, @vaccinations, @recommend, @purchased]
	end
end

class Magazine
	attr_reader :list
	def initialize
		@list  = Array.new
	end
	def add(name, kind, price, age, breed, vaccinations, recommend)
		@list << Animal.new(name, kind, price, age, breed, vaccinations, recommend)
	end
	def all
		@list.each do |i|
			puts i
		end
	end
	def buy_animal(id)
		@list[id - 1].purchased = true
	end
	def ret_animal(id)
		@list[id - 1].purchased = false
	end
	def info_animal(id)
		@list[id - 1]
	end

	def short_info(id)
		puts "#{id}. #{@list[id-1].name} (#{@list[id-1].kind}): #{@list[id-1].price}"
	end

	def all_info(id)
		puts "Name: #{@list[id-1].name} (#{@list[id-1].kind})"
		puts "Price: #{@list[id-1].price}"
		puts "Age: #{@list[id-1].age}"
		puts "Breed: #{@list[id-1].breed}"
		puts "Vaccinations: #{@list[id-1].vaccinations}"
		puts "Recommendations: #{@list[id-1].recommend}"
	end

	def info_all_animal
		@list.each_with_index do |it, ind|
			short_info(ind+1)
		end
	end

	def enter_number
		print "Enter the number of the animal: "
		id = gets.to_i 
		while id > @list.length
			print "The number is not correct enter again: "
			id = gets.to_i 
		end
		return id
	end
	
	def purchased
		return @list.count{|x| x.purchased}
	end

end

class Interface
	def initialize
		@magazine = Magazine.new
		@magazine.add("Barsik", "Dog", 500, 12, "Alabay", true, "Feed your puppy special food.")
		@magazine.add("Sirko", "Cat", 300, 11, "Sfinks", true, "Occasionally trim the tips of the claws, by means of special tweezers.")
		@magazine.add("Sharik", "Dog", 100, 10, "Dalmatian", true, "Find time to play with him.")
	end
	def menu
		system('cls')
		puts "\n*************************************************"
		puts "1. To buy the animal"
		puts "2. To return animal"
		puts "3. To view a list of purchased animals"
		puts "4. To view the list of available to buy animals"
		puts "5. To view information about the animal"
		puts "6. Progress bar"
		puts "7. Write data to a file"
		puts "*************************************************"
		print "Enter number: "
		id = gets.to_i
		case id
			when 1 then buy
			when 2 then ret
			when 3 then view_buy
			when 4 then view_free
			when 5 then view_info
			when 6 then progress_bar
			when 7 then write_to_file
			else puts "The number is not correct!"
		end
	end

	def buy
		@magazine.list.each_with_index do |it, ind|
			@magazine.short_info(ind+1) unless it.purchased
		end
		@magazine.buy_animal(@magazine.enter_number)
		puts "Successfully buy! \n"
	end

	def ret
		@magazine.list.each_with_index do |it, ind|
			@magazine.short_info(ind+1) if it.purchased
		end
		@magazine.ret_animal(@magazine.enter_number)
		puts "Successfully return! \n"
	end	

	def view_buy
		@magazine.list.each_with_index do |it, ind|
			@magazine.short_info(ind+1) if it.purchased
		end
	end

	def view_free
		@magazine.list.each_with_index do |it, ind|
			@magazine.short_info(ind+1) unless it.purchased
		end
	end

	def view_info
		@magazine.info_all_animal
		puts @magazine.all_info(@magazine.enter_number)
		sleep(2)
	end	

	def progress_bar
		p = 50 * @magazine.purchased / @magazine.list.length;
		puts "[" + ("|" * p) + (" " * (50-p)) + "]"
	end

	def write_to_file
		File.open("data.csv", "w") do |file|
			@magazine.list.each do |i|
				file.puts i.data.join(";")
			end
		end
		puts "Successfully written!"
	end
end

test = Interface.new
while true
	test.menu	
	sleep(0.7)
end
# test.all
# int.write_to_file(test)
